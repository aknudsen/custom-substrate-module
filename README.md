# Workshop Substrate Runtime Module
An example Substrate runtime module modeling arranging of workshops.

## Run
```./scripts/build.sh && cargo run --release -- --dev```

## Purge Chain State
```cargo run --release -- purge-chain --dev```

## Test
```
cargo test -p workshop-runtime workshop
```
