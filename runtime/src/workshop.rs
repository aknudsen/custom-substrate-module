use support::{
    decl_module, decl_storage, decl_event, StorageMap, StorageValue, dispatch::Result, ensure,
};
use parity_codec::{Encode, Decode};
use system::ensure_signed;
use support::traits::{Currency, WithdrawReason, ExistenceRequirement};
use runtime_primitives::traits::{Zero, Saturating};

pub trait Trait: balances::Trait {
    /// The overarching event type.
    type Event: From<Event<Self>> + Into<<Self as system::Trait>::Event>;
}

#[derive(Encode, Decode, Default, Clone, PartialEq, Debug)]
pub struct AnnouncedWorkshop<T: Trait> {
    organizer: T::AccountId,
    min_participants: u32,
    fee: T::Balance,
    leader: T::AccountId,
    leader_fee: T::Balance,
}

decl_storage! {
    trait Store for Module<T: Trait> as Workshop {
        Budget get(budget): T::Balance;
        Workshop get(announced): Option<AnnouncedWorkshop<T>>;
        // Lists must be modeled as integer indexed maps combined with a size property
        Participants: map u32 => T::AccountId;
        NumParticipants get(num_participants): u32;
    }
}

decl_module! {
    pub struct Module<T: Trait> for enum Call where origin: T::Origin {
        /// Initializing events
        fn deposit_event<T>() = default;

        /// Announce an impending workshop.
        pub fn announce_workshop(origin, leader: T::AccountId, leader_fee: T::Balance,
            min_participants: u32, fee: T::Balance) -> Result {
            let sender = ensure_signed(origin)?;

            ensure!(min_participants > 0, "there must be at least one participant");

            match <Workshop<T>>::get() {
                Some(_) => {
                    Err("a workshop is already underway")
                }
                None => {
                    <Workshop<T>>::put(AnnouncedWorkshop{
                        min_participants: min_participants, fee: fee, leader: leader.clone(),
                        leader_fee: leader_fee, organizer: sender.clone(),
                    });

                    Self::deposit_event(RawEvent::WorkshopAnnounced(sender, leader));

                    Ok(())
                }
            }
        }

        /// Participate in the currently announced workshop.
        /// The workshop fee is deducted from the sender's account.
        pub fn participate(origin) -> Result {
            let sender = ensure_signed(origin)?;
            let workshop = match <Workshop<T>>::get() {
                Some(workshop) => {
                    Ok(workshop)
                }
                None => {
                    Err("no workshop announced")
                }
            }?;

            let mut num_participants = <NumParticipants<T>>::get();

            /// Check if sender already participates in event
            for i in 0..num_participants {
                let acc_id = <Participants<T>>::get(i);
                if acc_id == sender {
                    return Ok(());
                }
            }

            let _ = <balances::Module<T> as Currency<_>>::withdraw(&sender, workshop.fee,
                WithdrawReason::Reserve, ExistenceRequirement::KeepAlive)?;

            <Participants<T>>::insert(num_participants, &sender);
            num_participants = num_participants.saturating_add(1);
            <NumParticipants<T>>::put(num_participants);
                
            let mut budget = <Budget<T>>::get();
            budget = budget.saturating_add(workshop.fee);
            <Budget<T>>::put(budget);

            Ok(())
        }

        /// Arrange the currently announced workshop.
        pub fn arrange_workshop(origin) -> Result {
            let sender = ensure_signed(origin)?;
            let workshop = match <Workshop<T>>::get() {
                Some(workshop) => {
                    Ok(workshop)
                }
                None => {
                    Err("no workshop announced")
                }
            }?;

            ensure!(sender == workshop.organizer, "you must be the organizer");

            let num_participants = <NumParticipants<T>>::get();
            ensure!(num_participants >= workshop.min_participants, "too few participants");

            let mut budget = <Budget<T>>::get();
            budget = budget.saturating_sub(workshop.leader_fee);
            let _ = <balances::Module<T> as Currency<_>>::deposit_creating(
                &workshop.leader, workshop.leader_fee);
            let _ = <balances::Module<T> as Currency<_>>::deposit_creating(
                &workshop.organizer, budget);

            Self::reset_workshop();

            Self::deposit_event(RawEvent::WorkshopHeld(workshop.organizer,
                num_participants));

            Ok(())
        }

        /// Cancel the currently announced workshop
        pub fn cancel_workshop(origin) -> Result {
            let sender = ensure_signed(origin)?;

            let workshop = match <Workshop<T>>::get() {
                Some(workshop) => {
                    Ok(workshop)
                }
                None => {
                    Err("no workshop announced")
                }
            }?;

            ensure!(sender == workshop.organizer, "you must be the organizer");

            let num_participants = <NumParticipants<T>>::get();
            for i in 0..num_participants {
                let participant = <Participants<T>>::get(i);
                let _ = <balances::Module<T> as Currency<_>>::deposit_into_existing(
                    &participant, workshop.fee)?;
            }

            Self::reset_workshop();

            Self::deposit_event(RawEvent::WorkshopCanceled(workshop.organizer));
            
            Ok(())
        }
    }
}

impl<T: Trait> Module<T> {
    fn reset_workshop() -> () {
        let zero: T::Balance = Zero::zero();
        <Budget<T>>::put(zero);
        <NumParticipants<T>>::put(0);
        <Workshop<T>>::kill();
    }
}

decl_event!(
    pub enum Event<T> where AccountId = <T as system::Trait>::AccountId {
        WorkshopAnnounced(AccountId, AccountId),
        WorkshopHeld(AccountId, u32),
        WorkshopCanceled(AccountId),
    }
);

#[cfg(test)]
mod tests {
    use super::*;

    use runtime_io::with_externalities;
    use primitives::{H256, Blake2Hasher};
    use support::{impl_outer_origin, assert_ok, assert_noop};
    use runtime_primitives::{
        BuildStorage,
        traits::{BlakeTwo256, IdentityLookup},
        testing::{Digest, DigestItem, Header}
    };

    impl_outer_origin! {
        pub enum Origin for WorkshopTest {}
    }

    #[derive(Clone, Eq, PartialEq, Debug)]
    pub struct WorkshopTest;

    impl system::Trait for WorkshopTest {
        type Origin = Origin;
        type Index = u64;
        type BlockNumber = u64;
        type Hash = H256;
        type Hashing = BlakeTwo256;
        type Digest = Digest;
        type AccountId = u64;
        type Lookup = IdentityLookup<Self::AccountId>;
        type Header = Header;
        type Event = ();
        type Log = DigestItem;
    }

    impl balances::Trait for WorkshopTest {
        type Balance = u64;
        type OnFreeBalanceZero = ();
        type OnNewAccount = ();
        type Event = ();
        type TransactionPayment = ();
        type TransferPayment = ();
        type DustRemoval = ();
    }

    impl super::Trait for WorkshopTest {
        type Event = ();
    }

    type Workshop = Module<WorkshopTest>;

    fn build_ext() -> runtime_io::TestExternalities<Blake2Hasher> {
        let mut t = system::GenesisConfig::<WorkshopTest>::default().build_storage().unwrap().0;
        t.extend(balances::GenesisConfig::<WorkshopTest>::default().build_storage().unwrap().0);
        t.into()
    }

    #[test]
    fn a_workshop_can_be_arranged() {
        with_externalities(&mut build_ext(), || {
            assert_ok!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 1, 12));
            // TODO: Check dispatched event
            let _ = <balances::Module<WorkshopTest> as Currency<_>>::deposit_creating(&12, 13);
            assert_ok!(Workshop::participate(Origin::signed(12)));

            assert_ok!(Workshop::arrange_workshop(Origin::signed(10)));

            assert_eq!(Workshop::budget(), 0);
            assert_eq!(Workshop::announced(), None);
            assert_eq!(Workshop::num_participants(), 0);
            let leader_bal = <balances::Module<WorkshopTest> as Currency<_>>::free_balance(&11);
            let organizer_bal = <balances::Module<WorkshopTest> as Currency<_>>::free_balance(&10);
            assert_eq!(leader_bal, 10);
            assert_eq!(organizer_bal, 2);

            // TODO: Check dispatched event
        });
    }

    #[test]
    fn a_workshop_can_only_be_arranged_if_enough_participants() {
        with_externalities(&mut build_ext(), || {
            assert_ok!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 1, 12));

            assert_noop!(Workshop::arrange_workshop(Origin::signed(10)), "too few participants");
        });
    }

    #[test]
    fn a_workshop_must_be_announced_before_arranging() {
        with_externalities(&mut build_ext(), || {
            assert_noop!(Workshop::arrange_workshop(Origin::signed(10)), "no workshop announced");
        });
    }

    #[test]
    fn a_workshop_can_only_be_held_by_the_organizer() {
        with_externalities(&mut build_ext(), || {
            assert_ok!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 1, 12));

            assert_noop!(Workshop::arrange_workshop(Origin::signed(11)),
                "you must be the organizer");
        });
    }

    #[test]
    fn one_cannot_participate_with_too_few_funds() {
        with_externalities(&mut build_ext(), || {
            assert_ok!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 1, 12));
            assert_noop!(Workshop::participate(Origin::signed(12)),
                "too few free funds in account");
        });
    }

    #[test]
    fn a_workshop_must_be_announced_before_participating() {
        with_externalities(&mut build_ext(), || {
            assert_noop!(Workshop::participate(Origin::signed(10)), "no workshop announced");
        });
    }

    #[test]
    fn participating_more_than_once_is_a_noop() {
        with_externalities(&mut build_ext(), || {
            assert_ok!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 1, 12));
            let _ = <balances::Module<WorkshopTest> as Currency<_>>::deposit_creating(&12, 13);
            assert_ok!(Workshop::participate(Origin::signed(12)));
            assert_ok!(Workshop::participate(Origin::signed(12)));
        });
    }

    #[test]
    fn a_workshop_requires_at_least_one_participant() {
        with_externalities(&mut build_ext(), || {
            assert_noop!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 0, 12),
                "there must be at least one participant");
        });
    }

    #[test]
    fn only_one_workshop_can_be_arranged_at_a_time() {
        with_externalities(&mut build_ext(), || {
            assert_ok!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 1, 12));

            assert_noop!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 1, 12),
                "a workshop is already underway");
        });
    }

    #[test]
    fn a_workshop_can_be_canceled() {
        with_externalities(&mut build_ext(), || {
            assert_ok!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 1, 12));
            let _ = <balances::Module<WorkshopTest> as Currency<_>>::deposit_creating(&12, 13);
            assert_ok!(Workshop::participate(Origin::signed(12)));

            assert_ok!(Workshop::cancel_workshop(Origin::signed(10)));

            assert_eq!(Workshop::budget(), 0);
            assert_eq!(Workshop::announced(), None);
            assert_eq!(Workshop::num_participants(), 0);
            let part_bal = <balances::Module<WorkshopTest> as Currency<_>>::free_balance(&12);
            let organizer_bal = <balances::Module<WorkshopTest> as Currency<_>>::free_balance(&10);
            assert_eq!(part_bal, 13);
            assert_eq!(organizer_bal, 0);
            // TODO: Check dispatched event

            assert_ok!(Workshop::announce_workshop(Origin::signed(10), 11, 10, 1, 12));
        });
    }

    #[test]
    fn a_workshop_must_be_announced_to_cancel() {
        with_externalities(&mut build_ext(), || {
            assert_noop!(Workshop::cancel_workshop(Origin::signed(10)), "no workshop announced");
        });
    }
}
